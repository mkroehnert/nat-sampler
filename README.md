# nat-sampler

Implementation of [nat-sampler](https://github.com/mafintosh/nat-sampler) in Rust.

## License

MIT (same as original)
